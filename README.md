# Программа по составлению подходящего логина

**Была написана на популярном в последнее время языке** *python*

В программе используются следующие возможности:

* Ввод пользователем имени и фамилии
* Проверка на наличие введеных символов на принадлежность к допустимым
* Проверка программы тестами

От пользователя получаем:

1. NAME
2. LASTNAME

А выводим:

1. LOGIN

Ниже будут представлены тесты:

```
class MyTest(unittest.TestCase):
    
    def test_Nikita_Mat(self):
        res = check_the_name("NIKITA","MAT")
        self.assertEqual(res, "NIKIM successfully registered")
       
    def test_Slava_Dm(self):
        res = check_the_name("SLAVA","DMIT")
        self.assertEqual(res, "SD successfully registered")


    def test_Nikita_Mat(self):
        res = check_the_name("","WFWEF")
        self.assertEqual(res, "Введено неверное значение")   
```

[Ссылка на весь код в gitlab](https://gitlab.com/SlavaDmitriev/vvid4/blob/master/vvid.py)

Алгоритм рабочего кода:

###### Cначала идёт проверка с допустимыми символами, а позже выполняется код:

```
log_l_n = last_name[0]
log_n = name[0]
for i in s:
        if AZ.find(i) < AZ.index(log_l_n):
            log_n += i
        else:
            break
```

![alt Картинка](https://www.pixelstalk.net/wp-content/uploads/2016/10/Image-of-Binary-Code-1.jpg)