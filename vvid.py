import unittest

AZ = str("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
def check_the_name(name, last_name):
    if (len(name.split()) == 1) and (len(last_name.split()) == 1):
        if name.isspace() or last_name.isspace():
            return ('Введено неверное значение')
        for i in name:
            if i not in AZ:
                return "Неверное значение"
        for j in last_name:
            if j not in AZ:
                return "Неверное значение"        
        else:
            index = 0
            s = name[:index] + name[index+1:]
            log_l_n = last_name[0]
            log_n = name[0]
            for i in s:
                if AZ.find(i) < AZ.index(log_l_n):
                    log_n += i
                else:
                    break
            return (log_n + log_l_n + " successfully registered")
    else:
    	return ('Введено неверное значение')
        
class MyTest(unittest.TestCase):
    
    def test_Nikita_Mat(self):
        res = check_the_name("NIKITA","MAT")
        self.assertEqual(res, "NIKIM successfully registered")
       
    def test_Slava_Dm(self):
        res = check_the_name("SLAVA","DMIT")
        self.assertEqual(res, "SD successfully registered")


    def test__(self):
        res = check_the_name("","WFWEF")
        self.assertEqual(res, "Введено неверное значение")     

        
if __name__ == "__main__":
    name = input('Введите ваше имя\n').upper()
    last_name = input('Введите вашу фамилию:\n').upper()
    print(check_the_name(name, last_name))
    unittest.main()